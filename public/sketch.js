/*
	Project 2
	Name of Project: Customizable Spaceship Shooting Game
	Author: GEUNYONG PARK
	Date: 2020.06.29
*/

// Preload Fonts
let robotoLight;
let robotoMedium;
let MuseoModerno;

function preload() {
	robotoLight = loadFont('data/Roboto_Light.otf');
	robotoMedium = loadFont('data/Roboto_Medium.otf');
	MuseoModerno = loadFont('data/MuseoModerno.otf');

	soundFormats('mp3', 'ogg');
	bgm = loadSound('data/bgm');
}


let bgm;

let bgmOnOff = false;

let logintoprofile = false;

let sendDataCheck = true;

let model = [];

let point = 0;

let palette;

let processingCheck = false;

let detect = false;

let currentPage;

var easycam;

let ws;


// Main Page Animation
let radius = 450;
let angle = 0;
let speed = 0.01;
let centerX = 610;
let centerY = 550;


function setup() {
	createCanvas(1440, 900, WEBGL);

	// Setting the easycam
	createEasyCam();
	document.oncontextmenu = () => false;
	easycam = new Dw.EasyCam(this._renderer, { distance: 500 });

	// Load the images
	imgMoon = loadImage('data/Moon.svg');
	imgMars = loadImage('data/Mars.svg');
	imgMoonSub = loadImage('data/Moon_Sub.svg');
	imgMarsSub = loadImage('data/Mars_Sub.svg');

	imgCircle500 = loadImage('data/Circle_500.svg');
	imgCircle700 = loadImage('data/Circle_700.svg');
	imgCircle900 = loadImage('data/Circle_900.svg');

	imgBgmOn = loadImage('data/Bgm_On.svg');
	imgBgmOff = loadImage('data/Bgm_Off.svg');

	imgBackOn = loadImage('data/Back_On.svg');
	imgBackOff = loadImage('data/Back_Off.svg');

	imgRightOn = loadImage('data/Right_On.svg');
	imgRightOff = loadImage('data/Right_Off.svg');
	imgLeftOn = loadImage('data/Left_On.svg');
	imgLeftOff = loadImage('data/Left_Off.svg');


	currentPage = mainPage;

	// Setting the innput
	userNameInput = createInput();
	passwordInput = createInput();
	userNameInput.hide();
	passwordInput.hide();

	// Color Palette for spaceship
	palette = [color(51), color(130), color(189),
	// black 100, 80, 60, 40, 20

	color(235, 87, 87), color(239, 120, 120), color(243, 154, 154),
	// red 100, 80, 60, 40, 20

	color(242, 153, 74), color(244, 173, 110), color(247, 193, 146),
	// orange 100, 80, 60, 40, 20

	color(241, 201, 76), color(244, 212, 112), color(247, 222, 147),
	// yellow 100, 80, 60, 40, 20

	color(33, 150, 83), color(77, 171, 117), color(121, 192, 151),
	// green 100, 80, 60, 40, 20

	color(47, 128, 237), color(88, 153, 240), color(130, 178, 244),
	// blue 100, 80, 60, 40, 20

	color(155, 81, 224), color(175, 116, 230), color(195, 150, 236)
		// purple 100, 80, 60, 40, 20]; 
	]

	// Setting for model array
	for (let h = 0; h < 3; h++) {
		let a = [];

		for (let x = 0; x < 7; x++) {
			let b = [];

			for (let y = 0; y < 7; y++) {
				b.push(-1);
			}
			a.push(b);
		}
		model.push(a);
	}
}


function draw() {
	background(35, 35, 47);
	translate(-width / 2, -height / 2, 0);

	bgmDraw();

	currentPage.draw();
}

function bgmDraw() {
	easycam.beginHUD();

	if (bgm.isPlaying()) {
		image(imgBgmOn, 1344, 80);
	}
	else {
		image(imgBgmOff, 1344, 80);
	}

	// BGM On Off Button & Functions
	if (bgmOnOff == true) {
		if ((mouseX > 1344 && mouseX < 1344 + 36) && (mouseY > 80 && mouseY < 80 + 36)) {
			bgmOnOff = false;
			if (bgm.isPlaying()) {
				bgm.stop();
			}
			else {
				bgm.setVolume(0.1);
				bgm.play();
			}
		}
	}

	easycam.endHUD();
}


function mousePressed() {
	currentPage.pressed()
	bgmOnOff = true;
	detect = true;
}


function mouseReleased() {
	bgmOnOff = false;
	detect = false;
}


// Main Page ------------------------------------------------------------------------------------------------------------------------

let mainPage = {
	draw: function () {
		mainPageHUD();
	},

	pressed: function () {
		if ((mouseX > 1108 && mouseX < 1108 + 120) && (mouseY > 308 && mouseY < 308 + 49)) {
			if (!logintoprofile) {
				currentPage = signinPage
				userNameInput.show();
				passwordInput.show();
			}
			else {
				firebase.database().ref('profile/' + firebase.auth().currentUser.uid).once("value", function (snapshot) {
					if (snapshot.exists()) {
						profilemodel = snapshot.val();
						currentPage = profilePage
					}
				})
			}
		}
		else if ((mouseX > 1108 && mouseX < 1108 + 159) && (mouseY > 475 && mouseY < 475 + 49)) {
			firebase.database().ref('ranking/').once("value", function (snapshot) {
				if (snapshot.exists()) {
					ranking = snapshot.val();
					currentPage = rankingsPage
				}
				else {
					ranking = [];
					currentPage = rankingsPage
				}
			})
		}
		else if ((mouseX > 1108 && mouseX < 1108 + 170) && (mouseY > 580 && mouseY < 580 + 49)) {
			firebase.database().ref('showcase/').once("value", function (snapshot) {
				if (snapshot.exists()) {
					showcase = snapshot.val();
					currentPage = showcasePage
					easycam.reset();
				}
				else {
					showcase = [];
					currentPage = showcasePage
					easycam.reset();
				}
			})
		}
		else if ((mouseX > 1108 && mouseX < 1108 + 248) && (mouseY > 715 && mouseY < 715 + 77)) {
		}
	}
}


function mainPageHUD() {
	easycam.beginHUD();

	image(imgCircle700, 200, 230);
	image(imgCircle900, 160, 100);

	image(imgMoon, 300, 330);

	let x = centerX + radius * cos(angle);
	let y = centerY + radius * sin(angle);
	image(imgMars, x - 80, y - 80);

	angle = angle + speed;

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Shooting Star', 80, 100);

	textFont(robotoLight, 36);
	fill(255, 255, 255);
	text('Customizable', 80, 690);

	textFont(MuseoModerno, 120);
	fill(255, 255, 255, 99);
	text('SPACESHIP', 76, 800);

	if (!logintoprofile) {
		// Sign In Button
		textFont(robotoMedium, 36);
		fill(255, 255, 255);
		if ((mouseX > 1108 && mouseX < 1108 + 120) && (mouseY > 308 && mouseY < 308 + 49)) {
			textFont(robotoMedium, 36)
			fill(255, 255, 255, 99);
		}
		text('Sign In', 1108, 338);
	}
	else {
		// Profile Button
		textFont(robotoMedium, 36);
		fill(255, 255, 255);
		if ((mouseX > 1103 && mouseX < 1103 + 120) && (mouseY > 308 && mouseY < 308 + 49)) {
			textFont(robotoMedium, 36)
			fill(255, 255, 255, 99);
		}
		text('Profile', 1103, 338);
	}

	// Rankings Button
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('How good are others', 1108, 470);

	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 1108 && mouseX < 1108 + 159) && (mouseY > 475 && mouseY < 475 + 49)) {
		textFont(robotoMedium, 36)
		fill(255, 255, 255, 99);
	}
	text('Rankings', 1108, 505);

	// Showcase Button
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('Show off your fancy spaceship', 1108, 575);

	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 1108 && mouseX < 1108 + 170) && (mouseY > 580 && mouseY < 580 + 49)) {
		textFont(robotoMedium, 36)
		fill(255, 255, 255, 99);
	}
	text('Showcase', 1108, 610);

	// Download Button
	fill(255, 255, 255, 99);
	if ((mouseX > 1108 && mouseX < 1108 + 248) && (mouseY > 713 && mouseY < 713 + 77)) {
		fill(255, 255, 255);
	}
	stroke(255);
	strokeWeight(3);
	rect(1108, 713, 228, 77, 10);

	noStroke();
	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 1108 && mouseX < 1108 + 248) && (mouseY > 713 && mouseY < 713 + 77)) {
		fill(35, 35, 47);
	}
	text('Download', 1140, 765);

	easycam.endHUD();
}


// Signin Page ------------------------------------------------------------------------------------------------------------------------

let signinPage = {
	draw: function () {
		signinPageHUD();
	},

	pressed: function () {
		if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
			let signinCheck = true;
			firebase.auth().signInWithEmailAndPassword(userNameInput.value(), passwordInput.value()).catch(function (error) {
				// Handle Errors here.
				let errorCode = error.code;
				let errorMessage = error.message;
				signinCheck = false;
				sendDataCheck = false;
				// ...
			}).then(() => {
				if (signinCheck) {
					currentPage = mainPage
					userNameInput.hide();
					passwordInput.hide();

					if (processingCheck) {
						// Save Profile data on server
						firebase.database().ref('profile/' + firebase.auth().currentUser.uid).child('model').set(model);
						firebase.database().ref('profile/' + firebase.auth().currentUser.uid).child('point').set(point);

						// Save Ranking data on server
						firebase.database().ref('ranking/').once("value", function (snapshot) {
							if (snapshot.exists()) {
								let ranking = snapshot.val();
								ranking.push({
									point: point,
									id: split(userNameInput.value(), '@')[0],
									model: model
								});
								ranking.sort(function (a, b) { return b.point - a.point });
								ranking.splice(12);
								firebase.database().ref('ranking/').set(ranking);
							}
							else {
								let ranking = [];
								ranking.push({
									point: point,
									id: split(userNameInput.value(), '@')[0],
									model: model
								});
								firebase.database().ref('ranking/').set(ranking);
							}
						})

						// Save Showcase on server
						firebase.database().ref('showcase/').once("value", function (snapshot) {
							if (snapshot.exists()) {
								let showcase = snapshot.val();
								showcase.reverse();
								showcase.push({
									point: point,
									id: split(userNameInput.value(), '@')[0],
									model: model
								});
								showcase.splice(12);
								showcase.reverse();
								firebase.database().ref('showcase/').set(showcase);
							}
							else {
								let showcase = [];
								showcase.push({
									point: point,
									id: split(userNameInput.value(), '@')[0],
									model: model
								});
								firebase.database().ref('showcase/').set(showcase);
							}
						})
					}

					logintoprofile = true;
					processingCheck = false;
				}
			})
		}
		else if ((mouseX > 664 && mouseX < 664 + 127) && (mouseY > 620 && mouseY < 620 + 49)) {
			currentPage = signupPage

		}
		else if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
			currentPage = mainPage
			userNameInput.hide();
			passwordInput.hide();
		}
	}
}


function signinPageHUD() {
	easycam.beginHUD();

	// noFill();
	// stroke(255);
	// strokeWeight(1);
	// line(90, 176, 90, 820);

	image(imgCircle500, 1100, 600);

	image(imgMarsSub, 1150, 650);

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Sign In', 80, 100);

	// Back Button
	image(imgBackOff, 80, 748);
	if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
		image(imgBackOn, 80, 748);
	}

	// User Name Input
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('User Name', 550, 259);

	noFill();
	stroke(255);
	strokeWeight(3);
	rect(550, 273, 340, 74, 10);

	userNameInput.position(560, 283);
	userNameInput.style("font-family", robotoLight);
	userNameInput.style('font-size', '36px');
	userNameInput.style('color', '#FFFFFF');
	userNameInput.style('background-color', '#23232F');
	userNameInput.size(340, 74);

	// Password Input
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('Password', 550, 374);

	noFill();
	stroke(255);
	strokeWeight(3);
	rect(550, 388, 340, 74, 10);

	passwordInput.position(560, 398);
	passwordInput.style("font-family", robotoLight);
	passwordInput.style('font-size', '36px');
	passwordInput.style('color', '#FFFFFF');
	passwordInput.style('background-color', '#23232F');
	passwordInput.size(340, 74);


	// Sign In Button
	fill(255, 255, 255, 99);
	if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
		fill(255, 255, 255);
	}
	stroke(255);
	strokeWeight(3);
	rect(550, 516, 340, 74, 10);

	noStroke();
	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
		fill(35, 35, 47);
	}
	text('Sign In', 665, 565);

	// Sign Up Button
	textFont(robotoLight, 36);
	fill(255, 255, 255);
	if ((mouseX > 664 && mouseX < 664 + 127) && (mouseY > 620 && mouseY < 620 + 49)) {
		textFont(robotoLight, 36)
		fill(255, 255, 255, 99);
	}
	text('Sign Up', 660, 650);

	easycam.endHUD();
}


// Signup Page ------------------------------------------------------------------------------------------------------------------------

let signupPage = {
	draw: function () {
		signupPageHUD();
	},

	pressed: function () {
		let signupCheck = true;
		if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
			firebase.auth().createUserWithEmailAndPassword(userNameInput.value(), passwordInput.value()).catch(function (error) {
				// Handle Errors here.
				let errorCode = error.code;
				let errorMessage = error.message;
				let signupCheck = false;
				// ...
			}).then(() => {
				if (signupCheck) {
					currentPage = signinPage
				}
			})
		}
		else if ((mouseX > 674 && mouseX < 674 + 127) && (mouseY > 620 && mouseY < 620 + 49)) {
			currentPage = signinPage
		}
		else if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
			currentPage = mainPage
			userNameInput.hide();
			passwordInput.hide();
		}
	}
}


function signupPageHUD() {
	easycam.beginHUD();

	// noFill();
	// stroke(255);
	// strokeWeight(1);
	// line(90, 176, 90, 820);

	image(imgCircle500, 1100, 600);

	image(imgMarsSub, 1150, 650);

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Sign Up', 80, 100);

	// Back Button
	image(imgBackOff, 80, 748);
	if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
		image(imgBackOn, 80, 748);
	}

	// User Name Input
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('User Name', 550, 259);

	noFill();
	stroke(255);
	strokeWeight(3);
	rect(550, 273, 340, 74, 10);

	userNameInput.position(560, 283);
	userNameInput.style("font-family", robotoLight);
	userNameInput.style('font-size', '36px');
	userNameInput.style('color', '#FFFFFF');
	userNameInput.style('background-color', '#23232F');
	userNameInput.size(340, 74);

	// Password Input
	textFont(robotoLight, 18);
	fill(255, 255, 255);
	text('Password', 550, 374);

	noFill();
	stroke(255);
	strokeWeight(3);
	rect(550, 388, 340, 74, 10);

	passwordInput.position(560, 398);
	passwordInput.style("font-family", robotoLight);
	passwordInput.style('font-size', '36px');
	passwordInput.style('color', '#FFFFFF');
	passwordInput.style('background-color', '#23232F');
	passwordInput.size(340, 74);

	// Sign Up Button
	fill(255, 255, 255, 99);
	if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
		fill(255, 255, 255);
	}
	stroke(255);
	strokeWeight(3);
	rect(550, 516, 340, 74, 10);

	noStroke();
	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 550 && mouseX < 550 + 340) && (mouseY > 516 && mouseY < 516 + 74)) {
		fill(35, 35, 47);
	}
	text('Sign Up', 665, 565);

	// Sign In Button
	textFont(robotoLight, 36);
	fill(255, 255, 255);
	if ((mouseX > 674 && mouseX < 674 + 127) && (mouseY > 620 && mouseY < 620 + 49)) {
		textFont(robotoLight, 36)
		fill(255, 255, 255, 99);
	}
	text('Sign In', 670, 650);

	easycam.endHUD();
}


// Profile Page ------------------------------------------------------------------------------------------------------------------------

let profilePage = {
	draw: function () {
		profilePageHUD();
	},

	pressed: function () {
		if ((mouseX > 674 && mouseX < 674 + 127) && (mouseY > 620 && mouseY < 620 + 49)) {
			setup();
		}
		else if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
			currentPage = mainPage
		}
	}
}

let profilemodel;

function profilePageHUD() {
	easycam.beginHUD();

	image(imgCircle500, 470, 600);

	image(imgMarsSub, 520, 650);

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Profile', 80, 100);

	// Back Button
	image(imgBackOff, 80, 748);
	if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
		image(imgBackOn, 80, 748);
	}

	let profilePoint = profilemodel.point
	let profileModel = profilemodel.model

	ambientLight(250, 250, 250);
	directionalLight(100, 100, 100, 50, 10, 30);

	push();
	loadBrick(profileModel, 320 + 300, 326 + 250)
	pop();

	noStroke();
	textFont(robotoLight, 36);
	fill(255, 255, 255);
	textAlign(CENTER, CENTER);
	text(userNameInput.value(), width / 2, height / 2 - 100);

	noStroke();
	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	textAlign(CENTER, CENTER);
	text(profilePoint, width / 2, height / 2 - 30);

	noStroke();
	textFont(robotoLight, 24);
	fill(255, 255, 255);
	textAlign(CENTER, CENTER);
	text('Press [F5] to sign out', width / 2, height / 2 - 150);

	easycam.endHUD();
}


// Ranking Page ------------------------------------------------------------------------------------------------------------------------

let rankingModelCount = 0;

let rankingsPage = {
	draw: function () {
		rankingsPageHUD();
	},

	pressed: function () {
		if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
			currentPage = mainPage
		}
		else if ((mouseX > 80 && mouseX < 80 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
			if (rankingModelCount > 0) rankingModelCount--;
		}
		else if ((mouseX > 1267 && mouseX < 1267 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
			if (rankingModelCount < 2) rankingModelCount++;
		}
	}
}


let ranking;

function rankingsPageHUD() {
	easycam.beginHUD();

	image(imgCircle500, 1100, 600);

	image(imgMarsSub, 1150, 650);

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Rankings', 80, 100);

	// Back Button
	image(imgBackOff, 80, 748);
	if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
		image(imgBackOn, 80, 748);
	}

	// Left Button
	image(imgLeftOff, 80, 414);
	if ((mouseX > 80 && mouseX < 80 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
		image(imgLeftOn, 80, 414);
	}

	// Right Button
	image(imgRightOff, 1267, 414);
	if ((mouseX > 1267 && mouseX < 1267 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
		image(imgRightOn, 1267, 414);
	}

	// Road Data from server

	ambientLight(250, 250, 250);
	directionalLight(100, 100, 100, 50, 10, 30);

	for (let i = 0; i < 2; i++) {
		for (let j = 0; j < 2; j++) {
			if (ranking[j + i * 2 + 4 * rankingModelCount] == undefined) break;
			let rankingsId = ranking[j + i * 2 + 4 * rankingModelCount].id
			let rankingsModel = ranking[j + i * 2 + 4 * rankingModelCount].model
			let rankingsPoint = ranking[j + i * 2 + 4 * rankingModelCount].point

			push();
			loadBrick(rankingsModel, 320 + j * 441, 326 + i * 222)
			pop();

			noStroke();
			textFont(robotoLight, 36);
			fill(255, 255, 255);
			text(rankingsId, 583 + j * 441, 370 + i * 222);

			noStroke();
			textFont(robotoMedium, 36);
			fill(255, 255, 255);
			text(rankingsPoint, 583 + j * 441, 420 + i * 222);


			noStroke();
			textFont(MuseoModerno, 48);
			fill(255, 255, 255);
			text(j + i * 2 + 4 * rankingModelCount + 1, 583 + j * 441, 310 + i * 222);
		}
	}


	easycam.endHUD();
}


// Showcase Page ------------------------------------------------------------------------------------------------------------------------

let showcaseModelCount = 0;

let showcase;

let showcasePage = {
	draw: function () {
		showcasePageHUD();
		ambientLight(250, 250, 250);
		directionalLight(100, 100, 100, 50, 10, 30);
		if (showcase[showcaseModelCount] == undefined) return;
		showcaseModel = showcase[showcaseModelCount].model

		push();
		loadBrick(showcaseModel, width / 2 - 100, height / 2 - 20);
		pop();
	},

	pressed: function () {
		//if (detect == true) {
		if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
			currentPage = mainPage
		}
		else if ((mouseX > 80 && mouseX < 80 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
			if (showcaseModelCount > 0) showcaseModelCount--;
			//detect = false;
		}
		else if ((mouseX > 1267 && mouseX < 1267 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
			if (showcaseModelCount < 11) showcaseModelCount++;
			//detect = false;
		}
		else if ((mouseX > 615 && mouseX < 615 + 248) && (mouseY > 747 && mouseY < 747 + 77)) {
			let msg = "";

			for (let h = 0; h < showcase[showcaseModelCount].model.length; h++) {
				for (let x = 0; x < showcase[showcaseModelCount].model[h].length; x++) {
					for (let y = 0; y < showcase[showcaseModelCount].model[h][x].length; y++) {
						let c = showcase[showcaseModelCount].model[h][x][y];
						if (c != -1) {
							msg += x + ",";
							msg += y + ",";
							msg += h + ",";
							msg += c + "\n";
						}
					}
				}
			}
			ws.send(msg);

			console.log(msg);
		}
	}
}



function showcasePageHUD() {
	easycam.beginHUD();

	// noFill();
	// stroke(255);
	// strokeWeight(1);
	// line(90, 176, 90, 820);

	image(imgCircle500, 1100, 600);

	image(imgMoonSub, 1150, 650);

	noStroke();
	textFont(MuseoModerno, 48);
	fill(255, 255, 255);
	text('Showcase', 80, 100);

	// Back Button
	image(imgBackOff, 80, 748);
	if ((mouseX > 80 && mouseX < 80 + 72) && (mouseY > 748 && mouseY < 748 + 72)) {
		image(imgBackOn, 80, 748);
	}

	// Left Button
	image(imgLeftOff, 80, 414);
	if ((mouseX > 80 && mouseX < 80 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
		image(imgLeftOn, 80, 414);
	}

	// Right Button
	image(imgRightOff, 1267, 414);
	if ((mouseX > 1267 && mouseX < 1267 + 93) && (mouseY > 414 && mouseY < 414 + 72)) {
		image(imgRightOn, 1267, 414);
	}

	// Download Button
	fill(255, 255, 255, 99);
	if ((mouseX > 615 && mouseX < 615 + 248) && (mouseY > 747 && mouseY < 747 + 77)) {
		fill(255, 255, 255);
	}
	stroke(255);
	strokeWeight(3);
	rect(615, 747, 228, 77, 10);

	noStroke();
	textFont(robotoMedium, 36);
	fill(255, 255, 255);
	if ((mouseX > 615 && mouseX < 615 + 248) && (mouseY > 747 && mouseY < 747 + 77)) {
		fill(35, 35, 47);
	}
	text('Download', 647, 799);

	easycam.endHUD();
}


// Web Socket ------------------------------------------------------------------------------------------------------------------------

// called when loading the page
$(function () {
	ws = new WebSocket("ws://localhost:8025/test");

	ws.onopen = function () {
		// Web Socket is connected, send data using send()
		console.log("Ready...");
	};

	ws.onmessage = function (evt) {
		var received_msg = evt.data;
		processingCheck = true;

		console.log("Message is received..." + received_msg);
		if (!sendDataCheck) {
			currentPage = profilePage

		}
		else {
			currentPage = signupPage
			userNameInput.show();
			passwordInput.show();
		}

		for (let h = 0; h < 3; h++) {
			for (let x = 0; x < 7; x++) {
				for (let y = 0; y < 7; y++) {
					model[h][x][y] = -1;
				}
			}
		}
		let spaceshipData = split(received_msg, "\n");
		console.log(spaceshipData);
		for (let a of spaceshipData) {
			if (a != "") {
				let b = split(a, ",");
				if (b.length == 1) {
					point = parseInt(b[0]);
				}
				else {
					console.log(a);
					console.log(b);
					console.log(model);
					model[parseInt(b[2])][parseInt(b[0])][parseInt(b[1])] = parseInt(b[3]);
				}
			}
		}
	};

	ws.onclose = function () {
		// websocket is closed.
		console.log("Connection is closed...");
	};
});



// Generating Spaceship ------------------------------------------------------------------------------------------------------------------------

function loadBrick() {
	noStroke();

	push();
	//translate(spaceshipx, spaceshipy, 0);
	translate(width / 2, height / 2);
	//rotateY(spaceshiprot);
	for (let h = 0; h < model.length; h++) {
		for (let x = 0; x < model[h].length; x++) {
			for (let y = 0; y < model[h][x].length; y++) {
				let c = model[h][x][y];
				if (c == -1) continue;

				scale(5);
				translate(x * 5, y * 5, h * 5);

				fill(palette[c]);
				box(4.8);

				translate(-x * 5, -y * 5, -h * 5);
				scale(0.2);
			}
		}
	}
	pop();
}


function loadBrick(model, deltaX, deltaY) {
	if (model == undefined) return;
	noStroke();

	push();
	translate(deltaX, deltaY);
	rotateX(PI / 6);
	rotateZ(-PI / 6);
	for (let h = 0; h < model.length; h++) {
		for (let x = 0; x < model[h].length; x++) {
			for (let y = 0; y < model[h][x].length; y++) {
				let c = model[h][x][y];
				if (c == -1) continue;

				scale(5);
				translate(x * 5, y * 5, h * 5);

				fill(palette[c]);
				box(4.8);

				translate(-x * 5, -y * 5, -h * 5);
				scale(0.2);
			}
		}
	}
	pop();
}