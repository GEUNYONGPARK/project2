int cols, rows;
int scl = 40;
int w = 2000;
int h = 1600;

float flying = 0;

float[][] terrain;

void drawTerrain() 
{

  flying -= 0.1;

  float yoff = flying;
  for (int y = 0; y < rows; y++) {
    float xoff = 0;
    for (int x = 0; x < cols; x++) {
      terrain[x][y] = map(noise(xoff, yoff), 0, 1, -500, 150);
      xoff += 0.2;
    }
    yoff += 0.2;
  }

  fill(255);
  //noFill();

  translate(width / 2, height / 2 + 50, -300);
  rotateX(PI / 3);
  translate(-w / 2, -h / 2);
  for (int y = 0; y < rows - 1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < cols; x++) {
      vertex(x * scl, y * scl, terrain[x][y]);
      vertex(x * scl, (y + 1) * scl, terrain[x][y+1]);
    }
    endShape();
  }
}