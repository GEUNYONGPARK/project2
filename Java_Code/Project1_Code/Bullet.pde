class Bullet 
{
    PVector bpos, bvel, bacc;
  

    Bullet(float x, float y) 
    {
        bpos = new PVector(x + 72.5, y);
        bacc = new PVector(0,-0.2);
        bvel = new PVector(0, -2.5);

    }
  

    void draw() 
    {
        bvel.add(bacc);
        bpos.add(bvel);
        
        fill(255, 229, 0);
        noStroke(); 
        pushMatrix();
        translate(0, 0, 100);     
        rect(bpos.x, bpos.y, 5, 30);  
        popMatrix();    
    }
}

void countBullet(int maxLength)
{
    while(bullets.size() > maxLength){
        bullets.remove(0); 
    }
}