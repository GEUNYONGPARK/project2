class Enemy
{
  PVector epos, evel, eacc;
  int r;
  color c;

  Enemy() 
  {
    epos = new PVector(random(width), random(-height, 0));
    eacc = new PVector(0, 0.005);
    evel = new PVector(0, 0);
    c = color(189);
  }

  Enemy pos(PVector p)
  {
    epos = p;
    return this; 
  }

  PVector pv()
  {
    return new PVector(epos.x, epos.y);
  }
  
  void update() 
  {
    evel.add(eacc);
    evel.limit(2);
    epos.add(evel);
    if (epos.y > height + 300) {
      epos.y = random(-400, 0);
    }
  }
  

  void draw() 
  {
    fill(c);
    noStroke();
    pushMatrix();
    translate(epos.x, epos.y, 100);
    rotate(frameCount / 50.0);
    sphere(80);
    popMatrix();
  }

  Enemy c(color c) 
  {
    this.c = c;
    return this;
  }

  boolean checkBomb(Bullet b) 
  {
    if(b.bpos.x <= epos.x + 160 && (b.bpos.x + 5) >= epos.x) {
  
      if((b.bpos.y + 30) >= epos.y && (b.bpos.y) <= epos.y + 160) {
        return true;
      } 
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
}

class EnemyFactory
{
  ArrayList<Enemy> getEnemy1() 
  {
    ArrayList<Enemy> enemies1 = new ArrayList<Enemy>();
    enemies1.add(new Enemy().c(color(130, 178, 244)));
    enemies1.add(new Enemy().pos(enemies1.get(0).pv().add(new PVector(80, 0))).c(color(130, 178, 244)));
    

    return enemies1;
  }

  ArrayList<Enemy> getEnemy2() 
  {
    ArrayList<Enemy> enemies2 = new ArrayList<Enemy>();
    enemies2.add(new Enemy().c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(80, 80))).c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(-80, 80))).c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(0, 160))).c(color(243, 154, 154)));

    return enemies2;
  }
}

