import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import controlP5.*; 
import processing.sound.*; 
import websockets.*; 
import java.net.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Project1_Code extends PApplet {

/*
	Project 1
	Name of Project: Customizable Spaceship Shooting Game
	Author: Geunyong Park
	Date: 2020.05.31
*/








WebsocketServer ws;

ControlP5 cp5;

SoundFile laserSound;
SoundFile bombSound;
SoundFile bgm;

PFont font;

PImage imgBackground;

int phase = 0;

int mh = 12;
int mw = 7;
int md = 7;
int current_h = 0;
int current_color = 1;

int enemiesNum = 10;
int life = 50;
int points = 0;

boolean detect;

boolean isReady = false;

String name;

ArrayList <Bullet> bullets;
ArrayList <Enemy> enemies;

Spaceship spaceship = new Spaceship();

EnemyFactory enemyfactory = new EnemyFactory();

public void settings() 
{
  size(1920, 1080, P3D);
}

public void setupModel() 
{
  model = new int[mh][mw][md];
  clearModel(model);
}


public void setup() 
{
  ws = new WebsocketServer(this, 8025, "/test");

  font = loadFont("ArialMT-64.vlw");

  setupGui();
  setupModel();

  bullets = new ArrayList<Bullet>();
  laserSound = new SoundFile(this, "laserSound.mp3");
  bombSound = new SoundFile(this, "bombSound.mp3");
  bgm = new SoundFile(this, "bgm.mp3");

  //bgm.amp(0.5);
  //bgm.play();

  imgBackground = loadImage("imgBackground.png");

  Spaceship spaceship = new Spaceship();

  enemies = new ArrayList<Enemy>();
  for (int i = 0; i < enemiesNum; i++) {
    enemies.add(new Enemy());
  }
  
  cols = w / scl;
  rows = h / scl;
  terrain = new float[cols][rows];
}


public void fillLayer(float x, float y, int h, int c) 
{
  float rectSize = 40;
  x = x - width / 4 * 3 - rectSize * 0.5f;
  y = y - height / 10 * 3 + rectSize * 5;
  x = x / rectSize;
  y = y / rectSize;
 
  int ix = round(x);
  int iy = round(y);
 
  if ( (ix > -1) && (ix < mw) && (iy > -1) && (iy < md) ) {
    model[h][ix][iy] = c;
  }
}


public void drawLayer(int h, int alpha) 
{
	float rectSize = 40;
  
	for(int x=0; x < mw; x++) {
    for(int y=0; y < md; y++) {
      int c = model[h][x][y];
    
      if (c==-1) {
        noFill();
        stroke(color(140, 140, 140));
        rect(width / 4 * 3 + rectSize * x, height / 10 + rectSize * y, rectSize, rectSize);
      } 
      else {
        noStroke();
        fill(palette[c], alpha);
        rect(width / 4 * 3 + rectSize * x, height / 10 + rectSize * y, rectSize, rectSize);
      }
    }
  }
}


public void drawLayerBackground() 
{
  fill(255, 255, 255, 200);
  noStroke();
  rect(width / 2, 0, width, height); 
}
  

public void draw() 
{
  background(35, 35, 47);

  if (phase == 0) {

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
  
    drawLayerBackground(); 

    paletteColorchip();

    if (current_h > 2) {
      for(int i=0; i < current_h - 2; i++) { 
        drawLayer(i, 30); 
      }
    }  
    if (current_h > 1) { 
      drawLayer(current_h - 2, 60); 
    }
    if (current_h > 0) { 
      drawLayer(current_h - 1, 120); 
    }

    drawLayer(current_h, 255);
    cp5.draw();

    float rot = 0;

    if (isReady) {
      rot = radians(millis() / 6 % 360);
    }

    positionBrick(model, palette, rot);
  }

  
  if (phase == 1) {

    //background(35, 35, 47);

    textFont(font);
    textSize(32);
    fill(255);
    text("SCORE:" + " " + points, width / 2 - 60, height / 18);
    textFont(font);
    textSize(32);
    fill(255);
    text("LIFE:" + " " + life, width / 2 - 60, height / 18 * 2);

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
    
    drawTerrain();

    for (int i = enemies.size() -1; i >= 0; i--) {
      Enemy e = enemies.get(i);
      e.update();
      e.draw();

      for (Bullet temp : bullets) {
        if((e.checkBomb(temp))) {
          if(enemies.size() > 0) {
            enemies.remove(i);
            points += 10;
            bombSound.play();
            if(enemies.size() >= enemiesNum) {

            }
            else if(random(0, 10) > 5) {
              enemies.add(new Enemy());
            }
            else if(random(0, 10) > 5) {
              ArrayList<Enemy> enemy1;
              enemy1 = enemyfactory.getEnemy1();
              for(Enemy enemy : enemy1) {
                enemies.add(enemy);
              }
            }
            else {
              ArrayList<Enemy> enemy2;
              enemy2 = enemyfactory.getEnemy2();
              for(Enemy enemy : enemy2) {
                enemies.add(enemy);
              }
            }
          }
        } 
      }

      if(spaceship.checkCollision(e)) {
        if(life > 0) {
          enemies.remove(i);
          life -= 10;
          bombSound.play();
          if(enemies.size() >= enemiesNum) {

          }
          else if(random(0, 10) > 5) {
            enemies.add(new Enemy());
          }
          else if(random(0, 10) > 5) {
            ArrayList<Enemy> enemy1;
            enemy1 = enemyfactory.getEnemy1();
            for(Enemy enemy : enemy1) {
              enemies.add(enemy);
            }
          }
          else {
            ArrayList<Enemy> enemy2;
            enemy2 = enemyfactory.getEnemy2();
            for(Enemy enemy : enemy2) {
              enemies.add(enemy);
            }
          }
        }
        else if(life <= 0) {
          phase = 2;
        }
      }
    }
  
    spaceship.draw();

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);

    for(Bullet temp : bullets) {
      //temp.move();
      temp.draw();
    }

    countBullet(30);
  }


   if (phase == 2) {

    textFont(font);
    textSize(64);
    fill(255);
    text("Your score is" + " " + points, width / 2, height / 2 - 50);


    // Send Data Button
    fill(255, 255, 255, 99);
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(255, 255, 255);
    }
    stroke(255, 255, 255);
    strokeWeight(3);
    rect(width / 2, height / 2 + 50 , 228, 77, 3);

    noStroke();
    textFont(font);
    textSize(36);
    fill(255, 255, 255);
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(35, 35, 47);
    }
    text("Send Data", width / 2 + 30, height / 2 + 100);

    senddataButton();

    // Retry Button
    fill(255, 255, 255, 99);
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(255, 255, 255);
    }
    stroke(255, 255, 255);
    strokeWeight(3);
    rect(width / 2 + 250, height / 2 + 50 , 148, 77, 3);

    noStroke();
    textFont(font);
    textSize(36);
    fill(255, 255, 255);
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(35, 35, 47);
    }
    text("Retry", width / 2 + 280, height / 2 + 100);

    retryButton();    

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
    
    float rot = 0;

    if (isReady) {
      rot = radians(millis() / 6 % 360);
    }
    positionBrick(model, palette, rot);
  }
}


public void mouseDragged() 
{
   fillLayer(mouseX, mouseY, current_h, current_color);
}


public void mousePressed() 
{
   fillLayer(mouseX, mouseY, current_h, current_color);
   detect = true;
}

public void mouseReleased() 
{
   detect = false;
}
  

public void keyPressed() 
{
  if (keyPressed) {
    if(key == ' ') {
      laserSound.play();
      
      Bullet temp = new Bullet(spaceship.spos.x, spaceship.spos.y);
      bullets.add(temp);
    }
  }
}


public void senddataButton()
{
  if (detect == true) {
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      String msg = "";
      String point = Integer.toString(points);
      msg+= point +"\n";

      for(int h=0; h < model.length; h++) {
        for(int x=0; x < model[h].length; x++) {
          for(int y=0; y < model[h][x].length; y++) { 
            int c = model[h][x][y];
            if (c != -1) {
              msg+= x +",";
              msg+= y +",";
              msg+= h +",";
              msg+= c +"\n";
            }
          }
        }
      }
      ws.sendMessage(msg);
      detect = false;
    }
  }
}

public void retryButton()
{
  if (detect == true) {
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      phase = 0;
      enemiesNum = 10;
      life = 50;
      points = 0;
      clearModel(model);
      spaceship = new Spaceship();

      detect = false;
    }
  }
}

// Load the spaceship on showcase from js

public void webSocketServerEvent(String msg) {
  println(msg);
  if (phase != 1) {
    for (int h = 0; h < 3; h++) {
			for (int x = 0; x < 7; x++) {
				for (int y = 0; y < 7; y++) {
					model[h][x][y] = -1;
				}
			}
		}
		String [] spaceshipData = split(msg, "\n");
		for (String a : spaceshipData) {
			if (a != "") {
				String [] b = a.split(",");
				model[parseInt(b[2])][parseInt(b[0])][parseInt(b[1])] = parseInt(b[3]);
			}
		}
  }
}



// your code down here
// feel free to crate other .pde files to organize your code
int cols, rows;
int scl = 40;
int w = 2000;
int h = 1600;

float flying = 0;

float[][] terrain;

public void drawTerrain() 
{

  flying -= 0.1f;

  float yoff = flying;
  for (int y = 0; y < rows; y++) {
    float xoff = 0;
    for (int x = 0; x < cols; x++) {
      terrain[x][y] = map(noise(xoff, yoff), 0, 1, -500, 150);
      xoff += 0.2f;
    }
    yoff += 0.2f;
  }

  fill(255);
  //noFill();

  translate(width / 2, height / 2 + 50, -300);
  rotateX(PI / 3);
  translate(-w / 2, -h / 2);
  for (int y = 0; y < rows - 1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < cols; x++) {
      vertex(x * scl, y * scl, terrain[x][y]);
      vertex(x * scl, (y + 1) * scl, terrain[x][y+1]);
    }
    endShape();
  }
}
class Bullet 
{
    PVector bpos, bvel, bacc;
  

    Bullet(float x, float y) 
    {
        bpos = new PVector(x + 72.5f, y);
        bacc = new PVector(0,-0.2f);
        bvel = new PVector(0, -2.5f);

    }
  

    public void draw() 
    {
        bvel.add(bacc);
        bpos.add(bvel);
        
        fill(255, 229, 0);
        noStroke(); 
        pushMatrix();
        translate(0, 0, 100);     
        rect(bpos.x, bpos.y, 5, 30);  
        popMatrix();    
    }
}

public void countBullet(int maxLength)
{
    while(bullets.size() > maxLength){
        bullets.remove(0); 
    }
}
public void clearModel(int[][][] m) 
{
  for(int z = 0; z < mh; z++) {
    for(int x = 0; x < mw; x++) {
      for(int y = 0; y < md; y++) {
        m[z][x][y] = -1; // no cube
      }
    }
  }
}


public void positionBrick(int[][][] model, int[] palette, float rot) 
{ 
  noStroke();
  pushMatrix();
  translate(width / 4, height / 2);
  rotateX(HALF_PI / 2);
  rotateY(rot);
  for(int h = 0; h < model.length; h++) {
    for(int x = 0; x < model[h].length; x++) {
      for(int y = 0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c == -1) continue;
      
        scale(5);
        translate(x * 5 - 20, y * 5 - 20, h * 5);

        fill(palette[c]);
        box(4.8f);
        
        translate(-x * 5 + 20, -y * 5 + 20, -h * 5);
        scale(0.2f);
      }
    }
  }
  popMatrix();
}


public int[][][] setCSV(Table t) 
{
  int[][][] m = new int[mh][mw][md];
  clearModel(m);
  
  for (TableRow r : t.rows()) {
    int x = r.getInt("x");
    int y = r.getInt("y");
    int z = r.getInt("z");
    int c = r.getInt("c");
    m[z][x][y] = c;
  }
  return m;
}


public Table getCSV(int[][][] model) 
{
  Table t = new Table();
  
  t.addColumn("x");
  t.addColumn("y");
  t.addColumn("z");
  t.addColumn("c");

    
  for(int h=0; h < model.length; h++) {
    for(int x=0; x < model[h].length; x++) {
      for(int y=0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c != -1) {
          TableRow r = t.addRow();
          r.setInt("x", x);
          r.setInt("y", y);
          r.setInt("z", h);
          r.setInt("c", c);
        }
      }
    }
  }
  return t;
}



class Enemy
{
  PVector epos, evel, eacc;
  int r;
  int c;

  Enemy() 
  {
    epos = new PVector(random(width), random(-height, 0));
    eacc = new PVector(0, 0.005f);
    evel = new PVector(0, 0);
    c = color(189);
  }

  public Enemy pos(PVector p)
  {
    epos = p;
    return this; 
  }

  public PVector pv()
  {
    return new PVector(epos.x, epos.y);
  }
  
  public void update() 
  {
    evel.add(eacc);
    evel.limit(2);
    epos.add(evel);
    if (epos.y > height + 300) {
      epos.y = random(-400, 0);
    }
  }
  

  public void draw() 
  {
    fill(c);
    noStroke();
    pushMatrix();
    translate(epos.x, epos.y, 100);
    rotate(frameCount / 50.0f);
    sphere(80);
    popMatrix();
  }

  public Enemy c(int c) 
  {
    this.c = c;
    return this;
  }

  public boolean checkBomb(Bullet b) 
  {
    if(b.bpos.x <= epos.x + 160 && (b.bpos.x + 5) >= epos.x) {
  
      if((b.bpos.y + 30) >= epos.y && (b.bpos.y) <= epos.y + 160) {
        return true;
      } 
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
}

class EnemyFactory
{
  public ArrayList<Enemy> getEnemy1() 
  {
    ArrayList<Enemy> enemies1 = new ArrayList<Enemy>();
    enemies1.add(new Enemy().c(color(130, 178, 244)));
    enemies1.add(new Enemy().pos(enemies1.get(0).pv().add(new PVector(80, 0))).c(color(130, 178, 244)));
    

    return enemies1;
  }

  public ArrayList<Enemy> getEnemy2() 
  {
    ArrayList<Enemy> enemies2 = new ArrayList<Enemy>();
    enemies2.add(new Enemy().c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(80, 80))).c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(-80, 80))).c(color(243, 154, 154)));
    enemies2.add(new Enemy().pos(enemies2.get(0).pv().add(new PVector(0, 160))).c(color(243, 154, 154)));

    return enemies2;
  }
}

int[] palette = 
{
  color(51), color(130), color(189), /*color(224), color(242),*/      
  // black 100, 80, 60, 40, 20

  color(235, 87, 87), color(239, 120, 120), color(243, 154, 154), /*color(246, 187, 187), color(250, 221, 221),*/      
  // red 100, 80, 60, 40, 20

  color(242, 153, 74), color(244, 173, 110), color(247, 193, 146), /*color(249, 214, 182), color(252, 234,218),*/      
  // orange 100, 80, 60, 40, 20

  color(241, 201, 76), color(244, 212, 112), color(247, 222, 147), /*color(249, 233, 183), color(252, 243, 218),*/      
  // yellow 100, 80, 60, 40, 20
  
  color(33, 150, 83), color(77, 171, 117), color(121, 192, 151), /*color(166, 212, 186), color(210, 233, 220),*/      
  // green 100, 80, 60, 40, 20

  color(47, 128, 237), color(88, 153, 240), color(130, 178, 244), /*color(171, 204, 247), color(213, 229, 251),*/      
  // blue 100, 80, 60, 40, 20

  color(155, 81, 224), color(175, 116, 230), color(195, 150, 236) /*color(214, 185, 242), color(234, 219, 248)*/   
  // purple 100, 80, 60, 40, 20
};


String palette_names[] = 
{ 
  "black100", "black80", "black60", /*"black40", "black20",*/
  "red100", "red80", "red60", /*"red40", "red20",*/
  "orange100", "orange80", "orange60", /*"orange40", "orange20",*/
  "yellow100", "yellow80", "yellow60", /*"yellow40", "yellow20",*/
  "green100", "green80", "green60", /*"green40", "green20",*/
  "blue100", "blue80", "blue60", /*"blue40", "blue20",*/
  "purple100", "purple80", "purple60" /*"purple40", "purple20"*/  
};


RadioButton selectColor;


public void setupGui() 
{
  cp5 = new ControlP5(this);
  PFont p = createFont("Arial",9); 
  ControlFont font = new ControlFont(p);
  cp5.setFont(font);
 
  cp5.setAutoDraw(false);
                
  selectColor = cp5.addRadioButton("selectC")
    .setPosition(width / 8 * 5 + 81, height / 10 + 31)
    .setSize(30, 30)
    //.setColorLabel(255)
    .setColorValue(255)
    .setColorActive(color(155))
    .setColorForeground(color(155))
    .setColorBackground(color(255)) 
    .addItem("black100", 0) 
    .addItem("black80", 1) 
    .addItem("black60", 2)
    .addItem("red100", 3)
    .addItem("red80", 4)
    .addItem("red60", 5)
    .addItem("orange100", 6) 
    .addItem("orange80", 7) 
    .addItem("orange60", 8)
    .addItem("yellow100", 9)
    .addItem("yellow80", 10)
    .addItem("yellow60", 11)
    .addItem("green100", 12)
    .addItem("green80", 13)
    .addItem("green60", 14)
    .addItem("blue100", 15)
    .addItem("blue80", 16)
    .addItem("blue60", 17)
    .addItem("purple100", 18)
    .addItem("purple80", 19)
    .addItem("purple60", 20);
     
  selectColor.activate(0);
  current_color = 2;

  cp5.addButton("Remove")
    .setPosition(width / 8 * 5 + 50, height / 10)
    .setSize(62, 30)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155));

  cp5.addSlider("Slider")
    .setPosition(width / 8 * 5, height / 10)
    .setSize(30,200)
    .setRange(0,2)
    .setNumberOfTickMarks(3)
    .setColorValue(155)
    .setColorActive(color(155))
    .setColorForeground(color(155))
    .setColorBackground(color(255))
    .setCaptionLabel(" ")
    .setLabelVisible(false);
     
  // cp5.addTextfield("NAME")
  //   .setPosition(width / 4 * 3, height / 10 * 3 + 100)
  //   .setSize(280, 80)
  //   .setAutoClear(false)
  //   .setColorLabel(255)
  //   .setCaptionLabel(" ")
  //   .setLabelVisible(false)
  //   .setColorBackground(color(255))
  //   .setFont(createFont("Arial",32));
   
  cp5.addButton("SUBMIT")
    .setPosition(width / 4 * 3, height / 10 * 3 + 200)
    .setSize(280, 80)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155))
    .setFont(createFont("Arial",32));
   
  cp5.addButton("START")
    .setPosition(width / 4 * 3, height / 10 * 3 + 300)
    .setSize(280, 80)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155))
    .setFont(createFont("Arial",32));
}


public void Slider(int a) 
{
   current_h = a;
}


public void selectC(int a) 
{
  current_color = a;
}


public void remove(int a) 
{
	if (a < 0) {
    current_color = -1;
  }
}


public void SUBMIT() 
{
   	isReady = !isReady;
   	//isReady = true;
   	Table t = getCSV(model);
   	saveTable(t, "data/spaceship.csv");
}

public void START() 
{
	isReady = true;
  phase = 1;
  Table t = loadTable("data/spaceship.csv", "header");
  model = setCSV(t);
  //name = cp5.get(Textfield.class,"NAME").getText();
}


public void paletteColorchip() 
{
  fill(51);
  rect(width / 8 * 5 + 50, height / 10 + 31, 30, 30);
  fill(130);
  rect(width / 8 * 5 + 50, height / 10 + 62, 30, 30);
  fill(189);
  rect(width / 8 * 5 + 50, height / 10 + 93, 30, 30);
  fill(235, 87, 87);
  rect(width / 8 * 5 + 50, height / 10 + 124, 30, 30);
  fill(239, 120, 120);
  rect(width / 8 * 5 + 50, height / 10 + 155, 30, 30);
  fill(243, 154, 154);
  rect(width / 8 * 5 + 50, height / 10 + 186, 30, 30);
  fill(242, 153, 74);
  rect(width / 8 * 5 + 50, height / 10 + 217, 30, 30);
  fill(244, 173, 110);
  rect(width / 8 * 5 + 50, height / 10 + 248, 30, 30);
  fill(247, 193, 146);
  rect(width / 8 * 5 + 50, height / 10 + 279, 30, 30);
  fill(241, 201, 76);
  rect(width / 8 * 5 + 50, height / 10 + 310, 30, 30);
  fill(244, 212, 112);
  rect(width / 8 * 5 + 50, height / 10 + 341, 30, 30);
  fill(247, 222, 147);
  rect(width / 8 * 5 + 50, height / 10 + 372, 30, 30);
  fill(33, 150, 83);
  rect(width / 8 * 5 + 50, height / 10 + 403, 30, 30);
  fill(77, 171, 117);
  rect(width / 8 * 5 + 50, height / 10 + 434, 30, 30);
  fill(121, 192, 151);
  rect(width / 8 * 5 + 50, height / 10 + 465, 30, 30);
  fill(47, 128, 237);
  rect(width / 8 * 5 + 50, height / 10 + 496, 30, 30);
  fill(88, 153, 240);
  rect(width / 8 * 5 + 50, height / 10 + 527, 30, 30);
  fill(130, 178, 244);
  rect(width / 8 * 5 + 50, height / 10 + 558, 30, 30);
  fill(155, 81, 224);
  rect(width / 8 * 5 + 50, height / 10 + 589, 30, 30);
  fill(175, 116, 230);
  rect(width / 8 * 5 + 50, height / 10 + 620, 30, 30);
  fill(195, 150, 236);
  rect(width / 8 * 5 + 50, height / 10 + 651, 30, 30);
}
int[][][] model;

class Spaceship 
{  
    PVector spos, svel, sacc;


    Spaceship() 
    {
        spos = new PVector(925, 1200, 0);
        svel = new PVector(0, 0, 0);
        sacc = new PVector(0, 0, 0);
    }


    public void draw() 
    {
        loadBrick(model, palette, spos.x, spos.y, -svel.x);
        //println(spos);
        svel.add(sacc);
        spos.add(svel);
        sacc.add(svel.mult(-0.04f));

        if (keyPressed) {
            if (key == CODED) {
                if (keyCode == UP) {
                    sacc.y = -5; 
                }

                else if (keyCode == DOWN){
                    sacc.y = +5; 
                }
                else if (keyCode == LEFT){
                    sacc.x = -5;
                    
                }
                else if (keyCode == RIGHT){
                    sacc.x = +5;                  
                }
            }
        }
    }

    public boolean checkCollision(Enemy e) 
    {
        // println(spos);
        // println(e.epos);
        if((spos.x <= e.epos.x + 80) && (spos.x + 240 >= e.epos.x + 80) && (spos.y <= e.epos.y) && (spos.y + 240 >= e.epos.y)) {
            return true;
        }    
        if((spos.x <= e.epos.x - 80) && (spos.x + 240 >= e.epos.x - 80) && (spos.y <= e.epos.y) && (spos.y + 240 >= e.epos.y)) {
            return true;
        }
        if((spos.x <= e.epos.x) && (spos.x + 240 >= e.epos.x) && (spos.y <= e.epos.y + 80) && (spos.y + 240 >= e.epos.y + 80)) {
            return true;
        }
        if((spos.x <= e.epos.x) && (spos.x + 240 >= e.epos.x) && (spos.y <= e.epos.y - 80) && (spos.y + 240 >= e.epos.y - 80)) {
            return true;
        }
        else {
            return false;
        }
    }
}


public void loadBrick(int[][][] model, int[] palette, float spaceshipx, float spaceshipy, float spaceshiprot) 
{
  noStroke();

  pushMatrix();
  translate(spaceshipx, spaceshipy, 0);
  translate(0, 0, 100);
  rotateY(spaceshiprot);
  for(int h = 0; h < model.length; h++) {
    for(int x = 0; x < model[h].length; x++) {
      for(int y = 0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c == -1) continue;
    
        scale(5);
        translate(x * 5, y * 5, h * 5);

        fill(palette[c]);
        box(4.8f);
        
        translate(-x * 5, -y * 5, -h * 5);
        scale(0.2f);
      }
    }
  }
  popMatrix();
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Project1_Code" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
