color[] palette = 
{
  color(51), color(130), color(189), /*color(224), color(242),*/      
  // black 100, 80, 60, 40, 20

  color(235, 87, 87), color(239, 120, 120), color(243, 154, 154), /*color(246, 187, 187), color(250, 221, 221),*/      
  // red 100, 80, 60, 40, 20

  color(242, 153, 74), color(244, 173, 110), color(247, 193, 146), /*color(249, 214, 182), color(252, 234,218),*/      
  // orange 100, 80, 60, 40, 20

  color(241, 201, 76), color(244, 212, 112), color(247, 222, 147), /*color(249, 233, 183), color(252, 243, 218),*/      
  // yellow 100, 80, 60, 40, 20
  
  color(33, 150, 83), color(77, 171, 117), color(121, 192, 151), /*color(166, 212, 186), color(210, 233, 220),*/      
  // green 100, 80, 60, 40, 20

  color(47, 128, 237), color(88, 153, 240), color(130, 178, 244), /*color(171, 204, 247), color(213, 229, 251),*/      
  // blue 100, 80, 60, 40, 20

  color(155, 81, 224), color(175, 116, 230), color(195, 150, 236) /*color(214, 185, 242), color(234, 219, 248)*/   
  // purple 100, 80, 60, 40, 20
};


String palette_names[] = 
{ 
  "black100", "black80", "black60", /*"black40", "black20",*/
  "red100", "red80", "red60", /*"red40", "red20",*/
  "orange100", "orange80", "orange60", /*"orange40", "orange20",*/
  "yellow100", "yellow80", "yellow60", /*"yellow40", "yellow20",*/
  "green100", "green80", "green60", /*"green40", "green20",*/
  "blue100", "blue80", "blue60", /*"blue40", "blue20",*/
  "purple100", "purple80", "purple60" /*"purple40", "purple20"*/  
};


RadioButton selectColor;


public void setupGui() 
{
  cp5 = new ControlP5(this);
  PFont p = createFont("Arial",9); 
  ControlFont font = new ControlFont(p);
  cp5.setFont(font);
 
  cp5.setAutoDraw(false);
                
  selectColor = cp5.addRadioButton("selectC")
    .setPosition(width / 8 * 5 + 81, height / 10 + 31)
    .setSize(30, 30)
    //.setColorLabel(255)
    .setColorValue(255)
    .setColorActive(color(155))
    .setColorForeground(color(155))
    .setColorBackground(color(255)) 
    .addItem("black100", 0) 
    .addItem("black80", 1) 
    .addItem("black60", 2)
    .addItem("red100", 3)
    .addItem("red80", 4)
    .addItem("red60", 5)
    .addItem("orange100", 6) 
    .addItem("orange80", 7) 
    .addItem("orange60", 8)
    .addItem("yellow100", 9)
    .addItem("yellow80", 10)
    .addItem("yellow60", 11)
    .addItem("green100", 12)
    .addItem("green80", 13)
    .addItem("green60", 14)
    .addItem("blue100", 15)
    .addItem("blue80", 16)
    .addItem("blue60", 17)
    .addItem("purple100", 18)
    .addItem("purple80", 19)
    .addItem("purple60", 20);
     
  selectColor.activate(0);
  current_color = 2;

  cp5.addButton("Remove")
    .setPosition(width / 8 * 5 + 50, height / 10)
    .setSize(62, 30)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155));

  cp5.addSlider("Slider")
    .setPosition(width / 8 * 5, height / 10)
    .setSize(30,200)
    .setRange(0,2)
    .setNumberOfTickMarks(3)
    .setColorValue(155)
    .setColorActive(color(155))
    .setColorForeground(color(155))
    .setColorBackground(color(255))
    .setCaptionLabel(" ")
    .setLabelVisible(false);
     
  // cp5.addTextfield("NAME")
  //   .setPosition(width / 4 * 3, height / 10 * 3 + 100)
  //   .setSize(280, 80)
  //   .setAutoClear(false)
  //   .setColorLabel(255)
  //   .setCaptionLabel(" ")
  //   .setLabelVisible(false)
  //   .setColorBackground(color(255))
  //   .setFont(createFont("Arial",32));
   
  cp5.addButton("SUBMIT")
    .setPosition(width / 4 * 3, height / 10 * 3 + 200)
    .setSize(280, 80)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155))
    .setFont(createFont("Arial",32));
   
  cp5.addButton("START")
    .setPosition(width / 4 * 3, height / 10 * 3 + 300)
    .setSize(280, 80)
    .setColorValue(155)
    .setColorActive(color(0))
    .setColorForeground(color(155))
    .setColorBackground(color(155))
    .setFont(createFont("Arial",32));
}


void Slider(int a) 
{
   current_h = a;
}


void selectC(int a) 
{
  current_color = a;
}


void remove(int a) 
{
	if (a < 0) {
    current_color = -1;
  }
}


void SUBMIT() 
{
   	isReady = !isReady;
   	//isReady = true;
   	Table t = getCSV(model);
   	saveTable(t, "data/spaceship.csv");
}

void START() 
{
	isReady = true;
  phase = 1;
  Table t = loadTable("data/spaceship.csv", "header");
  model = setCSV(t);
  //name = cp5.get(Textfield.class,"NAME").getText();
}


void paletteColorchip() 
{
  fill(51);
  rect(width / 8 * 5 + 50, height / 10 + 31, 30, 30);
  fill(130);
  rect(width / 8 * 5 + 50, height / 10 + 62, 30, 30);
  fill(189);
  rect(width / 8 * 5 + 50, height / 10 + 93, 30, 30);
  fill(235, 87, 87);
  rect(width / 8 * 5 + 50, height / 10 + 124, 30, 30);
  fill(239, 120, 120);
  rect(width / 8 * 5 + 50, height / 10 + 155, 30, 30);
  fill(243, 154, 154);
  rect(width / 8 * 5 + 50, height / 10 + 186, 30, 30);
  fill(242, 153, 74);
  rect(width / 8 * 5 + 50, height / 10 + 217, 30, 30);
  fill(244, 173, 110);
  rect(width / 8 * 5 + 50, height / 10 + 248, 30, 30);
  fill(247, 193, 146);
  rect(width / 8 * 5 + 50, height / 10 + 279, 30, 30);
  fill(241, 201, 76);
  rect(width / 8 * 5 + 50, height / 10 + 310, 30, 30);
  fill(244, 212, 112);
  rect(width / 8 * 5 + 50, height / 10 + 341, 30, 30);
  fill(247, 222, 147);
  rect(width / 8 * 5 + 50, height / 10 + 372, 30, 30);
  fill(33, 150, 83);
  rect(width / 8 * 5 + 50, height / 10 + 403, 30, 30);
  fill(77, 171, 117);
  rect(width / 8 * 5 + 50, height / 10 + 434, 30, 30);
  fill(121, 192, 151);
  rect(width / 8 * 5 + 50, height / 10 + 465, 30, 30);
  fill(47, 128, 237);
  rect(width / 8 * 5 + 50, height / 10 + 496, 30, 30);
  fill(88, 153, 240);
  rect(width / 8 * 5 + 50, height / 10 + 527, 30, 30);
  fill(130, 178, 244);
  rect(width / 8 * 5 + 50, height / 10 + 558, 30, 30);
  fill(155, 81, 224);
  rect(width / 8 * 5 + 50, height / 10 + 589, 30, 30);
  fill(175, 116, 230);
  rect(width / 8 * 5 + 50, height / 10 + 620, 30, 30);
  fill(195, 150, 236);
  rect(width / 8 * 5 + 50, height / 10 + 651, 30, 30);
}