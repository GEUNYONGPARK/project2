/*
	Project 1
	Name of Project: Customizable Spaceship Shooting Game
	Author: Geunyong Park
	Date: 2020.05.31
*/

import controlP5.*;
import processing.sound.*;

import websockets.*;
import java.net.*;


WebsocketServer ws;

ControlP5 cp5;

SoundFile laserSound;
SoundFile bombSound;
SoundFile bgm;

PFont font;

PImage imgBackground;

int phase = 0;

int mh = 12;
int mw = 7;
int md = 7;
int current_h = 0;
int current_color = 1;

int enemiesNum = 10;
int life = 50;
int points = 0;

boolean detect;

boolean isReady = false;

String name;

ArrayList <Bullet> bullets;
ArrayList <Enemy> enemies;

Spaceship spaceship = new Spaceship();

EnemyFactory enemyfactory = new EnemyFactory();

public void settings() 
{
  size(1920, 1080, P3D);
}

void setupModel() 
{
  model = new int[mh][mw][md];
  clearModel(model);
}


public void setup() 
{
  ws = new WebsocketServer(this, 8025, "/test");

  font = loadFont("ArialMT-64.vlw");

  setupGui();
  setupModel();

  bullets = new ArrayList<Bullet>();
  laserSound = new SoundFile(this, "laserSound.mp3");
  bombSound = new SoundFile(this, "bombSound.mp3");
  bgm = new SoundFile(this, "bgm.mp3");

  bgm.amp(0.5);
  bgm.play();

  imgBackground = loadImage("imgBackground.png");

  Spaceship spaceship = new Spaceship();

  enemies = new ArrayList<Enemy>();
  for (int i = 0; i < enemiesNum; i++) {
    enemies.add(new Enemy());
  }
  
  cols = w / scl;
  rows = h / scl;
  terrain = new float[cols][rows];
}


void fillLayer(float x, float y, int h, int c) 
{
  float rectSize = 40;
  x = x - width / 4 * 3 - rectSize * 0.5;
  y = y - height / 10 * 3 + rectSize * 5;
  x = x / rectSize;
  y = y / rectSize;
 
  int ix = round(x);
  int iy = round(y);
 
  if ( (ix > -1) && (ix < mw) && (iy > -1) && (iy < md) ) {
    model[h][ix][iy] = c;
  }
}


void drawLayer(int h, int alpha) 
{
	float rectSize = 40;
  
	for(int x=0; x < mw; x++) {
    for(int y=0; y < md; y++) {
      int c = model[h][x][y];
    
      if (c==-1) {
        noFill();
        stroke(color(140, 140, 140));
        rect(width / 4 * 3 + rectSize * x, height / 10 + rectSize * y, rectSize, rectSize);
      } 
      else {
        noStroke();
        fill(palette[c], alpha);
        rect(width / 4 * 3 + rectSize * x, height / 10 + rectSize * y, rectSize, rectSize);
      }
    }
  }
}


void drawLayerBackground() 
{
  fill(255, 255, 255, 200);
  noStroke();
  rect(width / 2, 0, width, height); 
}
  

public void draw() 
{
  background(35, 35, 47);

  if (phase == 0) {

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
  
    drawLayerBackground(); 

    paletteColorchip();

    if (current_h > 2) {
      for(int i=0; i < current_h - 2; i++) { 
        drawLayer(i, 30); 
      }
    }  
    if (current_h > 1) { 
      drawLayer(current_h - 2, 60); 
    }
    if (current_h > 0) { 
      drawLayer(current_h - 1, 120); 
    }

    drawLayer(current_h, 255);
    cp5.draw();

    float rot = 0;

    if (isReady) {
      rot = radians(millis() / 6 % 360);
    }

    positionBrick(model, palette, rot);
  }

  
  if (phase == 1) {

    //background(35, 35, 47);

    textFont(font);
    textSize(32);
    fill(255);
    text("SCORE:" + " " + points, width / 2 - 60, height / 18);
    textFont(font);
    textSize(32);
    fill(255);
    text("LIFE:" + " " + life, width / 2 - 60, height / 18 * 2);

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
    
    drawTerrain();

    for (int i = enemies.size() -1; i >= 0; i--) {
      Enemy e = enemies.get(i);
      e.update();
      e.draw();

      for (Bullet temp : bullets) {
        if((e.checkBomb(temp))) {
          if(enemies.size() > 0) {
            enemies.remove(i);
            points += 10;
            bombSound.play();
            if(enemies.size() >= enemiesNum) {

            }
            else if(random(0, 10) > 5) {
              enemies.add(new Enemy());
            }
            else if(random(0, 10) > 5) {
              ArrayList<Enemy> enemy1;
              enemy1 = enemyfactory.getEnemy1();
              for(Enemy enemy : enemy1) {
                enemies.add(enemy);
              }
            }
            else {
              ArrayList<Enemy> enemy2;
              enemy2 = enemyfactory.getEnemy2();
              for(Enemy enemy : enemy2) {
                enemies.add(enemy);
              }
            }
          }
        } 
      }

      if(spaceship.checkCollision(e)) {
        if(life > 0) {
          enemies.remove(i);
          life -= 10;
          bombSound.play();
          if(enemies.size() >= enemiesNum) {

          }
          else if(random(0, 10) > 5) {
            enemies.add(new Enemy());
          }
          else if(random(0, 10) > 5) {
            ArrayList<Enemy> enemy1;
            enemy1 = enemyfactory.getEnemy1();
            for(Enemy enemy : enemy1) {
              enemies.add(enemy);
            }
          }
          else {
            ArrayList<Enemy> enemy2;
            enemy2 = enemyfactory.getEnemy2();
            for(Enemy enemy : enemy2) {
              enemies.add(enemy);
            }
          }
        }
        else if(life <= 0) {
          phase = 2;
        }
      }
    }
  
    spaceship.draw();

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);

    for(Bullet temp : bullets) {
      //temp.move();
      temp.draw();
    }

    countBullet(30);
  }


   if (phase == 2) {

    textFont(font);
    textSize(64);
    fill(255);
    text("Your score is" + " " + points, width / 2, height / 2 - 50);


    // Send Data Button
    fill(255, 255, 255, 99);
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(255, 255, 255);
    }
    stroke(255, 255, 255);
    strokeWeight(3);
    rect(width / 2, height / 2 + 50 , 228, 77, 3);

    noStroke();
    textFont(font);
    textSize(36);
    fill(255, 255, 255);
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(35, 35, 47);
    }
    text("Send Data", width / 2 + 30, height / 2 + 100);

    senddataButton();

    // Retry Button
    fill(255, 255, 255, 99);
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(255, 255, 255);
    }
    stroke(255, 255, 255);
    strokeWeight(3);
    rect(width / 2 + 250, height / 2 + 50 , 148, 77, 3);

    noStroke();
    textFont(font);
    textSize(36);
    fill(255, 255, 255);
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      fill(35, 35, 47);
    }
    text("Retry", width / 2 + 280, height / 2 + 100);

    retryButton();    

    ambientLight(128, 128, 128);
    directionalLight(200, 200, 200, -5, -1, -3);
    
    float rot = 0;

    if (isReady) {
      rot = radians(millis() / 6 % 360);
    }
    positionBrick(model, palette, rot);
  }
}


void mouseDragged() 
{
   fillLayer(mouseX, mouseY, current_h, current_color);
}


void mousePressed() 
{
   fillLayer(mouseX, mouseY, current_h, current_color);
   detect = true;
}

void mouseReleased() 
{
   detect = false;
}
  

void keyPressed() 
{
  if (keyPressed) {
    if(key == ' ') {
      laserSound.play();
      
      Bullet temp = new Bullet(spaceship.spos.x, spaceship.spos.y);
      bullets.add(temp);
    }
  }
}


void senddataButton()
{
  if (detect == true) {
    if((mouseX > width / 2 && mouseX < width / 2 + 242) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      String msg = "";
      String point = Integer.toString(points);
      msg+= point +"\n";

      for(int h=0; h < model.length; h++) {
        for(int x=0; x < model[h].length; x++) {
          for(int y=0; y < model[h][x].length; y++) { 
            int c = model[h][x][y];
            if (c != -1) {
              msg+= x +",";
              msg+= y +",";
              msg+= h +",";
              msg+= c +"\n";
            }
          }
        }
      }
      ws.sendMessage(msg);
      detect = false;
    }
  }
}

void retryButton()
{
  if (detect == true) {
    if((mouseX > width / 2 + 250 && mouseX < width / 2 + 250 + 162) && (mouseY > height / 2 + 50 && mouseY < height / 2 + 50 + 77)) {
      phase = 0;
      enemiesNum = 10;
      life = 50;
      points = 0;
      clearModel(model);
      spaceship = new Spaceship();

      detect = false;
    }
  }
}

// Load the spaceship on showcase from js

void webSocketServerEvent(String msg) {
  println(msg);
  if (phase != 1) {
    for (int h = 0; h < 3; h++) {
			for (int x = 0; x < 7; x++) {
				for (int y = 0; y < 7; y++) {
					model[h][x][y] = -1;
				}
			}
		}
		String [] spaceshipData = split(msg, "\n");
		for (String a : spaceshipData) {
			if (a != "") {
				String [] b = a.split(",");
				model[parseInt(b[2])][parseInt(b[0])][parseInt(b[1])] = parseInt(b[3]);
			}
		}
  }
}



// your code down here
// feel free to crate other .pde files to organize your code