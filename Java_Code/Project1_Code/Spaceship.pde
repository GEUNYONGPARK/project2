int[][][] model;

class Spaceship 
{  
    PVector spos, svel, sacc;


    Spaceship() 
    {
        spos = new PVector(925, 1200, 0);
        svel = new PVector(0, 0, 0);
        sacc = new PVector(0, 0, 0);
    }


    void draw() 
    {
        loadBrick(model, palette, spos.x, spos.y, -svel.x);
        //println(spos);
        svel.add(sacc);
        spos.add(svel);
        sacc.add(svel.mult(-0.04));

        if (keyPressed) {
            if (key == CODED) {
                if (keyCode == UP) {
                    sacc.y = -5; 
                }

                else if (keyCode == DOWN){
                    sacc.y = +5; 
                }
                else if (keyCode == LEFT){
                    sacc.x = -5;
                    
                }
                else if (keyCode == RIGHT){
                    sacc.x = +5;                  
                }
            }
        }
    }

    boolean checkCollision(Enemy e) 
    {
        // println(spos);
        // println(e.epos);
        if((spos.x <= e.epos.x + 80) && (spos.x + 240 >= e.epos.x + 80) && (spos.y <= e.epos.y) && (spos.y + 240 >= e.epos.y)) {
            return true;
        }    
        if((spos.x <= e.epos.x - 80) && (spos.x + 240 >= e.epos.x - 80) && (spos.y <= e.epos.y) && (spos.y + 240 >= e.epos.y)) {
            return true;
        }
        if((spos.x <= e.epos.x) && (spos.x + 240 >= e.epos.x) && (spos.y <= e.epos.y + 80) && (spos.y + 240 >= e.epos.y + 80)) {
            return true;
        }
        if((spos.x <= e.epos.x) && (spos.x + 240 >= e.epos.x) && (spos.y <= e.epos.y - 80) && (spos.y + 240 >= e.epos.y - 80)) {
            return true;
        }
        else {
            return false;
        }
    }
}


void loadBrick(int[][][] model, color[] palette, float spaceshipx, float spaceshipy, float spaceshiprot) 
{
  noStroke();

  pushMatrix();
  translate(spaceshipx, spaceshipy, 0);
  translate(0, 0, 100);
  rotateY(spaceshiprot);
  for(int h = 0; h < model.length; h++) {
    for(int x = 0; x < model[h].length; x++) {
      for(int y = 0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c == -1) continue;
    
        scale(5);
        translate(x * 5, y * 5, h * 5);

        fill(palette[c]);
        box(4.8);
        
        translate(-x * 5, -y * 5, -h * 5);
        scale(0.2);
      }
    }
  }
  popMatrix();
}
