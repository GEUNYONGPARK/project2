## Personal information to add

* **Name**: GEUNYONG PARK
* **Student ID**: 20180232
* **email**: geun1524@kaist.ac.kr, geun15241234@gmail.com

## Table of content

* **Inspiration**
* **UML & Relationship between processing and p5.js** 
* **HOW TO PLAY** 
* **Inspiration** 
* **STRUCTURE**  

# Customizable Spaceship Shooting Game #

It's a shooting game that you can build and play your own spaceship. 
You can customize your spaceship and fight againt three types of enemies.
In processing, you can't see your ranking and share you spaceship with other.
But by using the p5.js I try to impreve that features.

You can enter the p5.js site with this link https://project-2-c4add.firebaseapp.com/

!!Processing takes about a minute to run.!!


---

## Inspiration
I was inspired by the movie Star Wars. The focus was to build your own spacecraft so that you can enjoy games and share them with others.

---

## UML & Relationship between processing and p5.js

![UML](images/1.png)
![Relationship](images/2.png)

---

## HOW TO PLAY
[for Processing]
![Processing1](images/3.png)
![Processing2](images/4.png)
![Processing3](images/5.png)

[for p5.js]
![p5.js1](images/6.png)
![p5.js2](images/7.png)
![p5.js3](images/8.png)
![p5.js1](images/9.png)
![p5.js2](images/10.png)
![p5.js3](images/11.png)

---

## LIBRARY
[For Processing]
- controlP5
    I use this for making GUI. Make RadioButton, Button, Slider.

- processing.sound
    My project is game! So I try to add sound with this library.


[For p5.js]
- p5.sound
    It supports sound. And I used if for make bgm. 

- p5.easycam
    Control the 3d object in space. Zoom in & Zoom out, Move and Rotate the object.

- Firebase
    Using the hosting export site as https://project-2-c4add.firebaseapp.com/.
    Using the authentification for making accounts.
    Using the database for saving account, spaceship info, point info.

![firebase1](images/20.png)
![firebase2](images/30.png) 
!! this images not loaded well please check images folder
There are 20.png for firebase 1 and 30.png for firebase 2


---

## SOURCES
I expanded the code that I made in __URP activities__ for a while and referred to the shooting game that we made at __Homework4__. 
And, terrain background came from shiffman's course. 
I take his course and applying that code in my game. I fixed that to make it suitable for my game.

---

## STRUCTURE
[For Processing]

My project consist of 3 Phases
`Customizing Spaceship Phase`, `Playing Game Phase`, `Score Phase`

To be specific, I divides as 7 tabs contain 3 classes. 
* Main code `Project1_Code` 
* `CreatCube` 
* `Background`
* `GUI`
* class `Spaceship` 
* class `Bullet` 
* class `Enemy`

[For p5.js]

I made 4 main function.
`Sign In`, `Sign Out`, `Profile`, `Rankings`, `Showcase`

---

### Processing_Code
This folder is main shows 3 phases.
Now I used phases to change the pages. But, I try to change it with state machine.

This phase consist of `setup()` and `draw()` to learn the entire game.


| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `fillLayer()`        | fill the grid by detecting the position with `mouseDragged()` and  `mousePressed()`
| `drawLayer()`        | Draw the grid that you can click or drag to draw your spaceship.     |



### CreatCube
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `clearModel()`       | It just clear the model                                              |
| `positionBrick()`    | It positioning the 3D cube on the right side of phase 1              |
| `setCSV()`           | It can save the data of spaceship as csv file                        |
| `getCSV()`           | It can load the csv data file                                        |



### Background
I think this code is not that meaningful. But, I really want to use it. Because its visual makes my games more active. 

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `drawTerrain()`      | It draws terrain under my game.                                      |



### GUI
It contains every controller used in game.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `setupGui()`         | Draw every controller that used for game control Such as RadioButton, Button, Slider, testFilled.         |
| `cp5.addRadioButton()`| Make color selecting button                                         |
| `slider()`           | Control the layer of grid                                            |
| `remove()`           | Remove the color from grid                                           |
| `SUBMIT()`           | Make model rotate and save model as csv file                         |
| `START()`            | Load saved model and play the shooting game                          |
| `paletteColorchip()` | Show the color of each palette                                       |



### class Spaceship
Our main feature in this game. 
This class is load the 'spaceship.csv' file and play on the game.
Such as control the spaceship and check the collision between enemies.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `draw()`             | Load the Spaceship and help to control it by using allow key         |
| `checkcoliision()`   | for every section in field, check if the section can make a new      |
| `loadBrick()`        | It load the spaceship csv file                                       |


### class Bullet
It makes bullets that spaceship can shoot toward the enemies

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `draw()`             | It draws the bullet and make it move toward the enemies              |
| `countBullet()`      | It counts the current bullet. if bullets exceed limit, it eliminates past bullets      |

### Enemy
It generates 3 types of enemies randomly. In this part I used __factory - design pattern__ for making diverse enemies.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `update()`           | It updates acc, vel, pos of enemies                                  |
| `draw()`             | This function produces the most basic form of enemy. This enemy is a one-ball.         |
| `checkBomb()`        | It detedct that enemies hit by bullets                               |
| `EnemyFactory()`     | It generates another types of enemies that comes from single ball basic enemies. enemy1 is two ball and enemy2 is 4 ball monsters                                     |

---

### p5.js_Code

### Main Page
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let mainPage`       | Setting the main page model                                              |
| `function mainPageHUD()`    | Draw the main page               |

### Sign In 
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let signinPagePage`       | Setting the signinPage page model                                              |
| `function signinPagePageHUD()`    | Draw the signinPage page               |

### Sign Out
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let signupPage`       | Setting the signup page model                                              |
| `function signupPageHUD()`    | Draw the signup page               |

### Profile
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let profilePage`       | Setting the profile page model                                              |
| `function profilePageHUD()`    | Draw the profile page               |

### Rankings
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let rankingsPage`       | Setting the rankings page model                                              |
| `function rankingsPageHUD()`    | Draw the rankings page               |

### Showcase
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `let showcasePage`       | Setting the showcase page model                                              |
| `function showcasePageHUD()`    | Draw the showcase page               |

### Generating Spaceship
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `function loadBrick(model, deltaX, deltaY)`       | Generating spaceship                                              |

---
Thank you for reading me.
I forgot the pull with Gihub Desktop Sorry for updating it late.
But I submitted other things in time. Thank you again.
